//Global vars

// Modules
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var readline = require('readline-sync');

//Functions
//--Combine, Minify and copy to production
//  This function combines and minifies CSS and JS files
//  and moves newly created files to production
//  (for the provided set of pages)
function combineMinify(page){
  return gulp.src('Dev/' + page + '.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('production'))
}

//Tasks
//--Compile pages
gulp.task('compile', function (){
  var page = readline.question("Input pages for compiling (enter for all pages):\n");
  if (page=="") { //Set to asterisk for all pages
    page="*";
  }
  else { //Convert input to array
  page = page.split(",");
  }
  for (i=0;i<page.length;i++) { //Compile each input separately
    combineMinify(page[i]);
  }
});

//--Sass
gulp.task('sass', function(){
  return gulp.src('Dev/assets/scss/**/*.scss')
    .pipe(sass()) // Sass module execution
    .pipe(gulp.dest('Dev/assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

//--Browser sync
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'Dev'
    },
  })
})

//--Watch
gulp.task('watch', ['browserSync', 'sass'], function(){
  gulp.watch('Dev/assets/scss/**/*.scss', ['sass']);
  gulp.watch('Dev/*.html', browserSync.reload);
  gulp.watch('Dev/assets/js/**/*.js', browserSync.reload);
})
